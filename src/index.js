import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { Values } from "redux-form-website-template";
import store from "./store";
import showResults from "./showResults";
import Wizard from "./forms/Wizard";
import Section from './forms/sections/Section';

import demographicFields from './forms/sections/demographics.fields';
import demograpificValidate from './forms/sections/demographics.validate';

import familyHistoryFields from './forms/sections/familyHistory.fields';

import tAndCFields from './forms/sections/tAndC.fields';
import tAndCValidate from './forms/sections/tAndC.validate';

import medQuestions from './forms/sections/medQuestions.fields';

import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';


const rootEl = document.getElementById("root");

ReactDOM.render(
  <Provider store={store}>
   <MuiThemeProvider muiTheme={getMuiTheme()}>
    <div style={{ padding: 15 }}>
      <h2>Intake form example</h2>
      <Wizard onSubmit={showResults}>
        <Section
            fields={demographicFields}
            form="wizard"
            validate={demograpificValidate}
          />
          <Section
            form="wizard"
            fields={familyHistoryFields}
          />
        <Section
          fields={medQuestions}
          form="wizard"
        />
        <Section
          fields={tAndCFields}
          form="wizard"
          validate={tAndCValidate}
        />
      </Wizard>
      <Values form="wizard" />
    </div>
    </MuiThemeProvider>
  </Provider>,
  rootEl
);