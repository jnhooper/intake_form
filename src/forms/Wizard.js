import React, { Component } from 'react';
import PropTypes from 'prop-types';
// import WizardFormFirstPage from './WizardFormFirstPage';
// import WizardFormSecondPage from './WizardFormSecondPage';
// import WizardFormThirdPage from './WizardFormThirdPage';

class Wizard extends Component {
  constructor(props) {
    super(props);
    this.nextPage = this.nextPage.bind(this);
    this.previousPage = this.previousPage.bind(this);
    this.state = {
      page: 0,
    };
  }
  nextPage() {
    console.log('wtf');
    this.setState({ page: this.state.page + 1 });
  }

  previousPage() {
    this.setState({ page: this.state.page - 1 });
  }

  render() {
    const { onSubmit, children } = this.props;
    const { page } = this.state;
    const self = this;
    const childrenWithProps = React.Children.map(children, (child, index) =>
      React.cloneElement(child, {
        onSubmit: () =>self.nextPage(),
        key: `section_${index}`,
        isFirst: index === 0,
        isLast: index === children.length-1,
        previousPage: () => self.previousPage(),
      }));
    return (
      <div>
        { children[page] &&
          childrenWithProps[page]
        }
      </div>
    );
  }
}

Wizard.propTypes = {
  onSubmit: PropTypes.func.isRequired,
};

export default Wizard;
