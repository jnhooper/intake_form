import React from 'react';
import TextField from 'material-ui/TextField';
import { RadioButton, RadioButtonGroup } from 'material-ui/RadioButton';
import Checkbox from 'material-ui/Checkbox';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import { DatePicker } from 'material-ui';

const renderTextField = (
  { input, label, meta: { touched, error }, ...custom },
) => (
  <TextField
    hintText={label}
    floatingLabelText={label}
    errorText={touched && error}
    {...input}
    {...custom}
  />
);

const renderCheckbox = ({ input, label }) => (
  <Checkbox
    label={label}
    checked={input.value ? true : false}
    onCheck={input.onChange}
  />
);

const renderRadioGroup = ({ input, type, ...rest, initialValue }) => {
  console.log('render', input, rest)
  return <RadioButtonGroup
    {...input}
    {...rest}
    onChange={(event, value) => input.onChange(value)}
  />
};

const renderSelectField = (
  { input, label, meta: { touched, error }, children, ...custom },
) => (
  <SelectField
    floatingLabelText={label}
    errorText={touched && error}
    {...input}
    onChange={(event, index, value) => input.onChange(value)}
    children={children}
    {...custom}
  />
);

const renderDateField = (
  { input, label, meta: { touched, error }, children, ...custom },
) => (
  <DatePicker
    hintText={label}
    mode="landscape"
    {...input}
    errorText={touched && error}
    onChange={(event, value) => input.onChange(value)}
  />
)


const renderField = (props) => {
  switch(props.type){
    case 'enum':
      return renderSelectField(props);
    case 'checkbox':
      return renderCheckbox(props);
    case 'radio':
      return renderRadioGroup(props);
    case 'date':
      return renderDateField(props);
    default:
    return renderTextField(props);
  }
};

export default renderField;