const questions = {
  smoke: {
    type: 'checkbox',
    label: 'I smoke',
    hasSubSection: true,
    validSubSections: {
      smokeList: {
        type: 'textarea',
        label: 'How much and how often?',
        required: true,
      }
    },
  },
  alcohol: {
    type: 'checkbox',
    label: 'I drink alcohol?',
    hasSubSection: true,
    validSubSections: {
      alcoholList: {
        type: 'textarea',
        label: 'How much and how often?',
        required: true,
      }
    }
  },
  illicitDrugs: {
    type: 'checkbox',
    label: 'I take illicit drugs',
    hasSubSection: true,
    validSubSections: {
      illicitDrugList: {
        type: 'textarea',
        label: 'How much and how often?',
        required: true,
      }
    },
  },
  currentMedications: {
    type: 'checkbox',
    label: 'I currently take medication',
    hasSubSection: true,
    validSubSections: {
      medicationList: {
        type: 'textarea',
        label: 'Please list any medication you are taking',
        required: true,
      }
    },
  },
  allergies: {
    type: 'checkbox',
    label: 'I have allergies',
    hasSubSection: true,
    validSubSections: {
        allergiesList:{
        type: 'textarea',
        label: 'Please list any allergies you may have',
        required: true,
      }
    },
  },
  previousHospitalStays: {
    type: 'checkbox',
    label: 'I have had previous hospital stays.',
    hasSubSection: true,
    validSubSections: {
      hospitalStayList: {
        type: 'textarea',
        label: 'Please list hospital stays you had, the date, and the reason for them',
        required: true,
      }
    },
  },
}

export default questions;
