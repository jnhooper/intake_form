const validate = values => {
  const errors = {};
  if(!values.termsAndConditions){
    errors.termsAndConditions = 'You must read'
  }
  return errors;
};

export default validate;
