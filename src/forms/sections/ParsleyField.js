import React from 'react';
import { Field } from 'redux-form';
import renderField from '../renderField';
import { RadioButton, MenuItem } from 'material-ui';


const ParsleyField = (props) => {
  const {type,
    index,
    label,
    values,
  } = props.field
  const name = props.name;
  const children = props.children;
  const onChange = props.onChange ? props.onChange : (e) => e; 
  let result = false;
  
  // const renderSelect = ({ input, meta: { touched, error } }) => (
  //   <div>
  //     <select {...input}>
  //       {values.map(val => <option value={val} key={val}>{val}</option>)}
  //     </select>
  //     {touched && error && <span>{error}</span>}
  //   </div>
  // );
  let options;

  switch(type){
    case 'enum': 
      options = values.map((value, i) => 
        <MenuItem value={value} primaryText={value} key={`${name}_enum_${i}`}/>
      )
      result = <Field
        name={name}
        component={renderField}
        type={type}
        label={label}
      >{
        options
      }
      </Field>
      break;
    case 'radio':
    console.log(values)
      options = values.map((value, i) =>{
        console.log(value);
        return <RadioButton
          key={`radio_${index}_${i}`}
          label={value.label}
          value={value.value}
        />
      }
      );
      result = <div>
        {label}
        <Field
          component={renderField}
          type={type}
          name={name}
          label={label}
        >
          {options}
        </Field>
      </div>;
      break;
    default:
      result = <Field
          key={`field_${index}`}
          onChange={(e)=>{
            onChange(e)
          }}
          name={name}
          type={type}
          component={renderField}
          label={label}
        />;
  }
  
  return <div>
    {result}
    {children &&
      <div>
        {children}
      </div>
    }
    </div>
}

export default ParsleyField;