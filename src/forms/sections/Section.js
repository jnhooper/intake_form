import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import ParsleyField from './ParsleyField';
import RaisedButton from 'material-ui/RaisedButton';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import ContentAdd from 'material-ui/svg-icons/content/add';

class Section extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fields: props.fields,
      additiveCount: 0,
    };
    this.addFields = this.addFields.bind(this);
    this.checkSubSection = this.checkSubSection.bind(this);
  }

  // add a field. used for additive fields
  addFields(fields, additiveName = false){
    const newFields = this.state.fields;
    const count = this.state.additiveCount;
    Object.keys(fields).forEach(field => {
      const name = additiveName ? `${additiveName}_${count}_${field}` :
        field;
      newFields[name] = fields[field];
    });
    
    this.setState({
      fields: newFields,
      additiveCount: count + 1
    })
  }

  checkSubSection(field, name, e){
    const {
      type,
    } = field;
    const newFields = this.state.fields;

    switch(type){
      case 'checkbox':
      default: 
        if(e.target.checked){
          newFields[name].visibleSubfields = field.validSubSections ?
            field.validSubSections : false
        } else {
          newFields[name].visibleSubfields = field.invalidSubSections ?
            field.invalidSubSections : false;
        }
      console.log(newFields[name]);
      this.setState({
        fields: newFields,
      })
      break;
    }
  }

  render() {
    const { handleSubmit,
      isFirst,
      isLast,
      previousPage,
      invalid } = this.props;
    const fields = this.state.fields;
    const sectionFields = Object.keys(fields).map((name, index) => {
      const field = fields[name];
      const {
        label,
        additive,
        repeatedFields,
        baseName,
        description,
        hasSubSection,
      } = field;
      const visibleSubfields = field.visibleSubfields ?
        field.visibleSubfields : false;
      let result = false;

      if(hasSubSection){
        result = <div>
          {description && description}
          <ParsleyField
            field={field}
            name={name}
            index={index}
            onChange={(e)=>{
              this.checkSubSection(field, name, e)
            }}
          >
          {
            visibleSubfields &&
            Object.keys(visibleSubfields).map((sf, i) => (
              <ParsleyField
                field={visibleSubfields[sf]}
                name={sf}
                index={i}
              />
            ))
          }
          </ParsleyField>
        </div>
      }
      else if(additive){
        result = <div>
          <h3>{label}</h3>
          {description && description}
            <br/>
            <br/>
            <FloatingActionButton 
              mini={true}
              onClick={(e)=>{
                e.preventDefault();
                this.addFields(repeatedFields, baseName)
              }
            }><ContentAdd /></FloatingActionButton>
          </div>
      }
      else {
        result = <div>
          {description &&
            description
          }
          <ParsleyField
            field={field}
            name={name}
            index={index}
         />
        </div>
      }
      return result;
    });

    // normally i would use css modules but dont want to spend time setting
    // it up right now
    const style={padding: '0.5rem'}
    const containerStyle={
      display: 'flex',
      flexDirection: 'row',
    }

    return (
      <form onSubmit={handleSubmit}>
      {sectionFields}
        <div style={containerStyle}>
          {!isFirst &&
            <div style={style}>
              <RaisedButton 
                type="button"
                className="previous"
                label="Back"
                onClick={previousPage}
                secondary={true}
              />
            </div>
        }
          {!isLast &&
            <div style={style}>
              <RaisedButton
                disabled={invalid}
                type="submit"
                label="Next"
                primary={true}
                className="next"
              />
            </div>
          }
          {
            isLast &&
            <div style={style}>
              <RaisedButton
                disabled={invalid}
                type="submit"
                label="Submit"
                primary={true}
                className="next"
              />
            </div>
          }
        </div>
      </form>
    );
  }
}

export default reduxForm({
  destroyOnUnmount: false, //        <------ preserve form data
  forceUnregisterOnUnmount: true, // <------ unregister fields on unmount
})(Section);
