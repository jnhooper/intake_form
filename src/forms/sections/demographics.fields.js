const fields = {
  firstName: {
    type: 'textInput',
    required: true,
    label: 'First Name',
  },
  lastName: {
    type: 'textInput',
    required: true,
    label: 'Last Name',
  },
  gender: {
    type: 'radio',
    required: true,
    values: [
      {
        label: 'Male',
        value: 'M'
      },
      {
        label: 'Female',
        value: 'F'
      }
    ],
    label: 'Birth Gender',
  },
  DOB: {
    type: 'date',
    required: false,
    label: 'Date of Birth',
  },
  email: {
    type: 'textInput',
    required: true,
    label: 'Email',
  },
  phoneNumber: {
    type: 'textInput',
    required: true,
    label: 'phoneNumber',
  },
  address1: {
    type: 'textInput',
    required: true,
    label: 'Address 1',
  },
  address2: {
    type: 'textInput',
    required: false,
    label: 'Address 2',
  },
  city: {
    type: 'textInput',
    required: true,
    label: 'City',
  },
  state: {
    type: 'textInput',
    required: true,
    label: 'state',
  },
  postalCode: {
    type: 'textInput',
    required: true,
    label: 'Zip Code',
  },
  martitalStatus: {
    type: 'enum',
    required: true,
    label: 'Marital Status',
    values: [
      'Married', 'Single', 'Divorced', 'Life Partner', 'Separated', 'Widowed', 'Other'
    ]
  },
}

export default fields;
