const diseases = [
  'Cancer',
  'Enjoying Pineapple on Pizza',
  'Heart Disease',
  'Diabetes',
  'Stroke',
  'High Blood Pressure',
  'High Cholesterol',
  'Liver Disease',
  'Alcohol / Drug Abuse',
  'Anxiety / Depression',
  'Tuberculosis',
  'Anesthesia Complications',
  'Genetic Disorder',
  'Hypertension',
  'Heart Attack',
  'Chronic Obstructive Pulmonary Disease',
  'Hepatitis',
  'Back Pain',
  'Psychotic disorder',
  'Irritable bowel syndrome',
  'Seizures',
  'Substance Abuse',
  'Depression',
  'Kidney Disease',
  'HIV',
  'Gastro esophageal reflux disease',
  'Thyroid disease',
  'Bipolar',
  'Eye disease',
  'Arthritis',
  'Asthma',
];

const relations = [
  'Grandparents',
  'Mother',
  'Father',
  'Siblings',
  'Children',
]

const familyHistory = {
  familyHistory: {
    additive: true,
    label: 'Family History',
    required: false,
    baseName: 'familyHistory',
    description: 'Please add any history of family illness',
    repeatedFields: {
      disease: {
        type: 'enum',
        values: diseases,
        label: 'Disease Name'
      },
      relation: {
        type: 'enum',
        values: relations,
        label: 'Relation',
      },
    }
  },
};

export default familyHistory;
export {
  relations,
  diseases,
  familyHistory,
}