const terms = {
  termsAndConditions:{
    description: `Lorem ipsum dolor amet messenger bag raw denim activated charcoal bushwick brunch pour-over tousled helvetica keffiyeh offal. Whatever butcher cliche, master cleanse ugh cronut hella mustache swag. Helvetica venmo chambray truffaut kickstarter neutra letterpress disrupt asymmetrical scenester narwhal heirloom. Schlitz butcher godard PBR&B raclette poutine scenester chicharrones snackwave 90's hell of. Vegan ugh copper mug fanny pack cold-pressed. Polaroid cardigan helvetica swag.

    Shaman listicle neutra, vegan vinyl health goth beard succulents hoodie tofu street art leggings. Mustache wayfarers truffaut schlitz neutra snackwave offal. Portland swag single-origin coffee post-ironic actually poke narwhal pug hella bushwick af coloring book. Try-hard hashtag forage normcore yr gluten-free 90's woke cornhole pork belly authentic mustache tousled umami. Wolf locavore 8-bit, biodiesel lyft four dollar toast snackwave kitsch lo-fi wayfarers taxidermy next level post-ironic dreamcatcher.`,
    type: 'checkbox',
    label: 'I have read and Agree to these terms',
    required: true,
  }
}

export default terms;